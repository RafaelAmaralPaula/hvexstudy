﻿using System;
using System.Globalization;
using ExampleOfConstructor.Model;

namespace ExampleOfConstructor
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            Console.WriteLine("Entre com dados de produtos : ");

    
            Console.Write("Nome : ");
            string name = Console.ReadLine();

            Console.Write("Preço : ");
            double price = double.Parse(Console.ReadLine() ,  CultureInfo.InvariantCulture);

            Console.Write("Quantidade em estoque : ");
            int quantity = int.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
        
            // SOBRECARGA DE CONSTRUTORES
            
            Product product = new Product(name , price , quantity);
            //Product product = new Product(name , price);

            Console.WriteLine();
            Console.WriteLine($"Dados do produto : " + product);

            Console.WriteLine();
            Console.WriteLine("Digite o número de produtos a ser adicionado ao estoque : ");
            int quantityUpdate = int.Parse(Console.ReadLine());
            product.AddProduct(quantityUpdate);

            Console.WriteLine("");
            Console.WriteLine("Dados atualizado: " + product.ToString());
            
            Console.WriteLine();
            Console.WriteLine("Digite o número de produtos a ser removido do estoque : ");
            quantityUpdate = int.Parse(Console.ReadLine());
            product.RemoveProduct(quantityUpdate);

            Console.WriteLine();
            Console.WriteLine("Dados atualizado : " + product.ToString());

            */

            int numberFour = 4;

            Console.WriteLine(numberFour++);//4
            Console.WriteLine(numberFour--);//5

            Console.WriteLine(++numberFour);//4
            Console.WriteLine(--numberFour);//5



        }
    }

}
