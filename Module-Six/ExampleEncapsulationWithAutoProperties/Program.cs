﻿using ExampleEncapsulationWithAutoProperties.Model;
using System;
using System.Globalization;


namespace ExampleEncapsulationWithAutoProperties
{
    class Program
    {
        static void Main(string[] args)
        {
             Product product = new Product();
            
            Console.WriteLine("Nome :");
            product.Name = Console.ReadLine();


            Console.WriteLine("Preço :");
            product.Price = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);


            Console.WriteLine("Nome -> " + product.Name + " , R$" + product.Price.ToString("F2" , CultureInfo.InvariantCulture) + " , " + product.Quantity + " unidade");

            Console.WriteLine("Quantos você quer adicionar no estoque:");
            int quantity = int.Parse(Console.ReadLine());
            product.AddProduct(quantity);

            Console.WriteLine("Dados atualizado : " + product);
            

        }   
    }

}

