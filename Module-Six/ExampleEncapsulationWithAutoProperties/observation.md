# Erro na questão de tipos anuláveis na autoproperties

## Solução
- Ir até arquivo ExampleEncapsulationWithAutoProperties.csproj é arquivo **XML**
- Encontrar tag <Nullable>enabled</Nullable> para <Nullable>disabled</Nullable>
- Com isso poderá trabalhar com valores podendo ser null

## Link de referência
[Referência](https://stackoverflow.com/questions/67505347/non-nullable-property-must-contain-a-non-null-value-when-exiting-constructor-co)


