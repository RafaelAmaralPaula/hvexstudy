using System;
using System.Globalization;

namespace ExampleEncapsulationWithAutoProperties.Model
{
    class Product
    {
        public string Name{ get; set;}
        public double Price{get; set;}
        public int Quantity{get; private set;}

    
         public double TotalValueStock()
        {
            return Price * Quantity;
        }

        public void AddProduct(int quantity)
        {
            Quantity += quantity;
        }

        public void RemoveProduct(int quantity)
        {
            Quantity -= quantity;
        }

        public override string ToString()
        {
            return Name 
            + ", $" 
            + Price.ToString("F2" , CultureInfo.InvariantCulture) 
            + ", "
            + Quantity
            + " unidades , Total : $"
            + TotalValueStock().ToString("F2" , CultureInfo.InvariantCulture);
        }

    }
}