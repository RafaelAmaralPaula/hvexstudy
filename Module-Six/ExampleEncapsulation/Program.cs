﻿using System;
using System.Globalization;
using ExampleEncapsulation.Model;

namespace ExampleEncapsulation
{
    class Program
    {
        static void Main(string[] args)
        {   
            Product product = new Product();

            Console.WriteLine("Entre com dados de produtos : ");

            Console.Write("Nome : ");
            product.SetName(Console.ReadLine());

            Console.Write("Preço : ");
            product.SetPrice(double.Parse(Console.ReadLine() ,  CultureInfo.InvariantCulture));

            
            Console.WriteLine();
            Console.WriteLine($"Dados do produto : " + product);

            Console.WriteLine();
            Console.WriteLine("Digite o número de produtos a ser adicionado ao estoque : ");
            int quantity = int.Parse(Console.ReadLine());
            product.AddProduct(quantity);

            Console.WriteLine("");
            Console.WriteLine("Dados atualizado: " + product.ToString());
            
            Console.WriteLine();
            Console.WriteLine("Digite o número de produtos a ser removido do estoque : ");
            quantity = int.Parse(Console.ReadLine());
            product.RemoveProduct(quantity);

            Console.WriteLine();
            Console.WriteLine("Dados atualizado : " + product.ToString());

        }

    }
}