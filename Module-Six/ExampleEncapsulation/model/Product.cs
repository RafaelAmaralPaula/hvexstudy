using System;
using System.Globalization;

namespace ExampleEncapsulation.Model
{
    class Product
    {
        private string _name;
        private double _price;
        private int _quantity;

        public Product()
        {
            _quantity = 0;
        }

        public Product(string name , double price) : this()
        {
            _name = name;
            _price = price;

        }

        public string GetName()
        {
            return _name;
        }

        public void SetName(string name)
        {
            _name = name;
        }

        public double GetPrice()
        {
            return _price;
        }

        public void SetPrice(double price)
        {
            _price = price;
        }   

        public int GetQuantity()
        {
            return _quantity;
        }

        public double TotalValueStock()
        {
            return _price * _quantity;
        }

        public void AddProduct(int quantity)
        {
            _quantity += quantity;
        }

        public void RemoveProduct(int quantity)
        {
            _quantity -= quantity;
        }

        public override string ToString()
        {
            return _name 
            + ", $" 
            + _price.ToString("F2" , CultureInfo.InvariantCulture) 
            + ", "
            + _quantity
            + " unidades , Total : $"
            + TotalValueStock().ToString("F2" , CultureInfo.InvariantCulture);
        }


    }
}