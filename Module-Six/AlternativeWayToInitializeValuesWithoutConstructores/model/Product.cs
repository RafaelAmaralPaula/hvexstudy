using System;
using System.Globalization;

namespace AlternativeWayToInitializeValuesWithoutConstructores.Model
{
    class Product
    {

        public string Name;
        public double Price;
        public int Quantity;

        public Product(){
            Quantity = 0;
        }

        public Product(string name , double price) : this()
        {
            this.Name = name;
            this.Price = price;
        }

        public Product(string name , double price , int quantity): this(name , price)
        {
            this.Quantity = quantity;

        }

        public double TotalValueStock()
        {
            return Price * Quantity;
        }

        public void AddProduct(int quantity)
        {
            Quantity += quantity;
        }

        public void RemoveProduct(int quantity)
        {
            Quantity -= quantity;
        }

        public override string ToString()
        {
            return Name 
            + ", $" 
            + Price.ToString("F2" , CultureInfo.InvariantCulture) 
            + ", "
            + Quantity
            + " unidades , Total : $"
            + TotalValueStock().ToString("F2" , CultureInfo.InvariantCulture);
        }

    }

}
