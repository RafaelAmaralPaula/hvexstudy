﻿using System;
using System.Globalization;
using AlternativeWayToInitializeValuesWithoutConstructores.Model;

namespace AlternativeWayToInitializeValuesWithoutConstructores
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entre com dados de produtos : ");

            Console.Write("Nome : ");
            string name = Console.ReadLine();

            Console.Write("Preço : ");
            double price = double.Parse(Console.ReadLine() ,  CultureInfo.InvariantCulture);

            Console.Write("Quantidade em estoque : ");
            int quantity = int.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
        
           
            Product product = new Product(name , price , quantity);
           
           // Product product = new Product();

           // Product product = new Product(name , price);

            /*
            Product product = new Product(){
                Name = name,
                Price = price,
                Quantity = quantity
           };
           */
           

           /*
           Product product = new Product{
                Name = name,
                Price = price,
                Quantity = quantity
           };
           */
            

            Console.WriteLine();
            Console.WriteLine($"Dados do produto : " + product);

            Console.WriteLine();
            Console.WriteLine("Digite o número de produtos a ser adicionado ao estoque : ");
            int quantityUpdate = int.Parse(Console.ReadLine());
            product.AddProduct(quantityUpdate);

            Console.WriteLine("");
            Console.WriteLine("Dados atualizado: " + product.ToString());
            
            Console.WriteLine();
            Console.WriteLine("Digite o número de produtos a ser removido do estoque : ");
            quantityUpdate = int.Parse(Console.ReadLine());
            product.RemoveProduct(quantityUpdate);

            Console.WriteLine();
            Console.WriteLine("Dados atualizado : " + product.ToString());
        }
    }
}