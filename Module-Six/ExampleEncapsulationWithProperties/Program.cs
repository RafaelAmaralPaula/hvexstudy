﻿using System;
using System.Globalization;
using ExampleEncapsulationWithProperties.Model;

namespace ExampleEncapsulationWithProperties{
    class Program
    {
        static void Main(string[] args)
        {

            Product product = new Product();
            
            Console.WriteLine("Nome :");
            product.Name = Console.ReadLine();

            Console.WriteLine("Preço :");
            product.Price = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);

            Console.WriteLine("Quantidade :");
            product.Quantity = int.Parse(Console.ReadLine());

            Console.WriteLine("Nome -> " + product.Name + " , R$" + product.Price.ToString("F2" , CultureInfo.InvariantCulture) + " , " + product.Quantity + " unidade");

        }

    }
}
