using System;
using System.Globalization;

namespace ExampleEncapsulationWithProperties.Model
{
    class Product
    {
        private string _name;
        private double _price;
        private int _quantity;


        public string Name
        {
            get{return _name;}
            set{
                _name = value;
            }
        }

        public double Price
        {
            get{return _price;}
            set{
                _price = value;
            }
        }

        public int Quantity{
            get{return _quantity;}
            set{
                _quantity = value;
            }
        }

        public double TotalValueStock()
        {
            return Price * Quantity;
        }

        public void AddProduct(int quantity)
        {
            _quantity += quantity;
        }

        public void RemoveProduct(int quantity)
        {
            _quantity -= quantity;
        }

        public override string ToString()
        {
            return Name 
            + ", $" 
            + Price.ToString("F2" , CultureInfo.InvariantCulture) 
            + ", "
            + Quantity
            + " unidades , Total : $"
            + TotalValueStock().ToString("F2" , CultureInfo.InvariantCulture);
        }


    }
}