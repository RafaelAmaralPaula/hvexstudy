﻿using System;

namespace TypeBasicsPartOne
{
    class Program
    {
        static void Main(string[] args)
        {
            byte numberOne = 126;
            int numberTwo = 1000;
            int numberThree = 2147483647;
            long numberFour = 2147483648L;
            float numberFive = 4.5F;
            double numberSix = 4.90;

            Console.WriteLine(numberOne);
            Console.WriteLine(numberTwo);
            Console.WriteLine(numberThree);
            Console.WriteLine(numberFour);    
            Console.WriteLine(numberFive);
            Console.WriteLine(numberSix);


            bool logicalValue = true;
            char women = 'F';
            char male = '\u0041';
            string name = "José Da Silva";
            object objectOne = "Fernando Oliveira";
            object numberSeven = 5.689;
            
            Console.WriteLine(logicalValue);
            Console.WriteLine(women);
            Console.WriteLine(male);
            Console.WriteLine(name);
            Console.WriteLine(objectOne);
            Console.WriteLine(numberSeven);

            // Min and max of type basics

            int numberOneIntMin = int.MinValue;
            int numberTwoIntMax = int.MaxValue;

            Console.WriteLine(numberOneIntMin);
            Console.WriteLine(numberTwoIntMax);

            sbyte numberOneSbyteMin = sbyte.MinValue;
            sbyte numberTwoSbyteMax = sbyte.MaxValue;

            Console.WriteLine(numberOneSbyteMin);
            Console.WriteLine(numberTwoSbyteMax);

            decimal numberOneDecimalMin = decimal.MinValue;
            decimal numberTwoDecimalMax = decimal.MaxValue;

            Console.WriteLine(numberOneDecimalMin);
            Console.WriteLine(numberTwoDecimalMax);

        

        }

    }
}
