﻿namespace ExampleFile
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var directory = new DirectoryInfo("folder-test");

            if (!directory.Exists)
            {
                directory.Create();

            }

            var path = directory.Name + "/test.text";
            var path2 = directory.Name + "/test2.text";
            var file = new FileInfo(path);
            //if(File.Exists(path+"teste.txt"));
            if (!file.Exists)
            {
                file.CreateText();
                File.CreateText(path2);
            }

            Console.WriteLine("Nome arquivo : " + file.Name);
            Console.WriteLine("Nome completo do arquivo : " + file.FullName);
            // Console.WriteLine("Tamnaho do arquivo : " + file.Length);
            Console.WriteLine("Última data de alteração : " + file.LastAccessTime);
            Console.WriteLine("Nome do riretório onde arquivo está arquivo : " + file.DirectoryName);

            file.MoveTo(path2, true);

        }
    }
}
