﻿namespace ExampleDataTime
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var now = DateTime.Now;
            var today = DateTime.Today;

            Console.WriteLine("Now : " + now);
            Console.WriteLine("Today: " + today);

            var threeDayAgo = today.AddDays(-3);
            var sixMouthLater = today.AddMonths(6);
            var twoYearsLater = today.AddYears(2);

            Console.WriteLine(threeDayAgo);
            Console.WriteLine(sixMouthLater);
            Console.WriteLine(twoYearsLater);

            Console.WriteLine("----------------------------------");
            Console.WriteLine("ShortDate and ShortTime ");

            var shortDate = now.ToShortDateString();
            var shortTime = now.ToShortTimeString();

            Console.WriteLine("ShortDate: " + shortDate);
            Console.WriteLine("ShortTime: " + shortTime);

            Console.WriteLine("----------------------------------");
            Console.WriteLine("LongDate and LongTime ");

            var longDate = now.ToLongDateString();
            var longTime = now.ToLongTimeString();

            Console.WriteLine("LongDate: " + longDate);
            Console.WriteLine("LongTime: " + longTime);


            Console.WriteLine("----------------------------------");
            Console.WriteLine("Property date");

            var day = now.Day;
            var month = now.Month;
            var years = now.Year;

            Console.WriteLine("Day : " + day);
            Console.WriteLine("Month : " + month);
            Console.WriteLine("Years : " + years);


            Console.WriteLine("----------------------------------");
            Console.WriteLine("Property time");
            var hour = now.Hour;
            var minute = now.Minute;
            var second = now.Second;

            Console.WriteLine("Hour : " + hour);
            Console.WriteLine("Minute : " + minute);
            Console.WriteLine("Second : " + second);

            Console.WriteLine("----------------------------------");
            var dayOfWeek = now.DayOfWeek;
            if (dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
            {
                Console.WriteLine("Final de semana!");
            }
            else
            {
                Console.WriteLine("Infezlimente não é final de semana");
            }

            var dayOfYears = now.DayOfYear;
            Console.WriteLine("Day of years: " + dayOfYears);

        }
    }
}
