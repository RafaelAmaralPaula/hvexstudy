﻿using ExampleLanguageIntegratedQuery.Util;

namespace ExampleLanguageIntegratedQuery
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("ExampleAny");
            StudentUtil.ExampleAny();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleSingle");
            StudentUtil.ExampleSingle();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleSingleOrDefaul");
            StudentUtil.ExampleSingleOrDefault();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleFirst");
            StudentUtil.ExampleFirst();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleFirstOrDefault");
            StudentUtil.ExampleFirstOrDefault();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleOrderByGrade");
            StudentUtil.ExampleOrderByGrade();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("OrderByGradeDescending");
            StudentUtil.ExampleOrderByGradeDescending();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleWhere");
            StudentUtil.ExampleWhere();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleSelec");
            StudentUtil.ExampleSelect();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleSelectMany");
            StudentUtil.ExampleSelectMany();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleMaxGrad");
            StudentUtil.ExampleMaxGrade();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleMinGrad");
            StudentUtil.ExampleMinGrade();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleSumGrade");
            StudentUtil.ExampleSumGrade();
            Console.WriteLine("---------------------------------");

            Console.WriteLine("ExampleCountGradeMinFourteen");
            StudentUtil.ExampleCountGradeMinFourteen();


        }
    }
}

