using ExampleLanguageIntegratedQuery.Model;

namespace ExampleLanguageIntegratedQuery.Service
{
    public class StudentService
    {

        public static List<Student> CreateStudent()
        {
            return new List<Student>{
                new Student(1 , "José da silva" , "672.175.590-49" , 100),
                new Student(2 , "Ricardo oliveira" , "081.963.410-75" , 35),
                new Student(3 , "Bianca silva" , "951.378.640-48" , 85),
                new Student(4 , "Manoel francisco" , "913.444.590-05" , 70),
                new Student(5 , "Vanessa pires" , "082.222.910-25" , 75),
                new Student(6 , "Lucas braga" , "403.661.710-97" , 60)
            };

        }

    }
}