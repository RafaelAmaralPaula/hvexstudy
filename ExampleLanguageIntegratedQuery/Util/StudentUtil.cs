using ExampleLanguageIntegratedQuery.Model;
using ExampleLanguageIntegratedQuery.Service;

namespace ExampleLanguageIntegratedQuery.Util
{
    public class StudentUtil
    {

        private static List<Student> _list = StudentService.CreateStudent();

        //Any
        public static void ExampleAny()
        {
            var any = _list.Any();
            var anyGradeEqualsHundred = _list.Any(result => result.Grade == 100);

            Console.WriteLine(any);
            Console.WriteLine(anyGradeEqualsHundred);
        }

        //Single 
        public static void ExampleSingle()
        {
            var single = _list.Single(result => result.Id == 1);
            Console.WriteLine(single);
        }


        //SingleOrDefault
        public static void ExampleSingleOrDefault()
        {
            var singleOrDefault = _list.Single(result => result.Document == "672.175.590-49");
            Console.WriteLine(singleOrDefault);
        }

        // Firts 
        public static void ExampleFirst()
        {
            var first = _list.First(result => result.FullName == "José da silva");
            Console.WriteLine(first);
        }

        //FirstOrDefault
        public static void ExampleFirstOrDefault()
        {
            var firstOrDefault = _list.FirstOrDefault(result => result.Grade == 0);
            Console.WriteLine(firstOrDefault);
        }

        //Order By
        public static void ExampleOrderByGrade()
        {
            var orderBy = _list.OrderBy(result => result.Grade);
            orderBy.ToList().ForEach(result => Console.WriteLine(result));

        }

        //Order descending
        public static void ExampleOrderByGradeDescending()
        {
            var orderByGradeDescending = _list.OrderByDescending(result => result.Grade);
            orderByGradeDescending.ToList().ForEach(result => Console.WriteLine(result));
        }

        //Where
        public static void ExampleWhere()
        {
            var where = _list.Where(result => result.Grade < 70);
            where.ToList().ForEach(result => Console.WriteLine(result));

        }

        //Select
        public static void ExampleSelect()
        {
            var select = _list.Select(result => result.Grade);
            select.ToList().ForEach(result => Console.WriteLine(result));

        }

        //SelectMany
        public static void ExampleSelectMany()
        {
            var selectMany = _list.SelectMany(result => result.PhoneNumbers);
            selectMany.ToList().ForEach(result => Console.WriteLine(result));

        }

        //Sum
        public static void ExampleSumGrade()
        {
            var sum = _list.Sum(result => result.Grade);
            Console.WriteLine(sum);
        }

        //Max
        public static void ExampleMaxGrade()
        {
            var max = _list.Max(result => result.Grade);
            Console.WriteLine(max);
        }

        //Min
        public static void ExampleMinGrade()
        {
            var min = _list.Min(result => result.Grade);
            Console.WriteLine(min);
        }

        //Count
        public static void ExampleCountGradeMinFourteen()
        {
            var count = _list.Count();
            Console.WriteLine(count);
        }
    }
}