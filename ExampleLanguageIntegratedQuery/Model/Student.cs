namespace ExampleLanguageIntegratedQuery.Model
{
    public class Student
    {

        public int Id { get; set; }

        public string FullName { get; set; }

        public string Document { get; set; }

        public int Grade { get; set; }

        public List<string> PhoneNumbers { get; set; }

        public Student() { }
        public Student(int id, string fullName, string document, int grade)
        {
            Id = id;
            FullName = fullName;
            Document = document;
            Grade = grade;

            PhoneNumbers = new List<string> { "(11) 99477-6989", "(69) 92484-2381", "(21) 91343-4356" };
        }


        public override string ToString()
        {
            return "Id: " + Id + " , Name: " + FullName +
                   " , Document : " + Document + " , Grade: " + Grade;
        }


    }
}