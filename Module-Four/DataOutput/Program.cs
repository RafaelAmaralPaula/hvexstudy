﻿using System;
using System.Globalization;

namespace DataOutput
{
    class Program
    {
        static void Main(string[]args)
        {
            Console.Write("Good morning!");
            Console.WriteLine("Good Afternoon!");
            Console.WriteLine("Good night!");

            Console.WriteLine("---------------------------------------------");
            
            string name = "João Paulo";
            char genre = 'F';
            int age = 20;
            double balance = 10.35784;

            Console.WriteLine(name);
            Console.WriteLine(genre);
            Console.WriteLine(age);
            Console.WriteLine(balance);
            Console.WriteLine(balance.ToString("F2"));
            Console.WriteLine(balance.ToString("F4"));

            //IMPRIME IGNORANDO QUAIS QUER FORMATAÇÃO DE UM DETERMIANDO PAÍS
            Console.WriteLine(balance.ToString("F3" , CultureInfo.InvariantCulture));

            Console.WriteLine("---------------------------------------------");
            
            //PLACEHOLDER
            Console.WriteLine("{0} tem {1} anos e tem saldo igual a R${2:F2} reais.", name , age , balance);

            //INTERPOLAÇÃO
            Console.WriteLine($"{name} tem {age} anos e tem saldo igual a R${balance:F2} reais!");

            //CONCATENAÇÃO
            Console.WriteLine(name + " tem " + age + " anos e tem saldo igual a R$" + balance.ToString("F2" , CultureInfo.InvariantCulture) + " reais..." );

        }
    }

}