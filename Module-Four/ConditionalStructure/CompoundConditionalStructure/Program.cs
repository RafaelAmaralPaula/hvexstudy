﻿using System;

namespace CompoundConditionalStructure
{
    class Program
    {
        static void Main(string[] args) {

            Console.WriteLine("Digite um número inteiro");
            int value = int.Parse(Console.ReadLine());

            bool result = value % 2 == 0;
            if(result)
            {
                Console.WriteLine("O número {0} é PAR!" , value);
            }
            else
            {
                Console.WriteLine($"O número {value} é IMPAR!");
            }


        }

    }
}
