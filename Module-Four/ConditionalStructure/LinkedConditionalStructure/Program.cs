﻿using System;

namespace LinkedConditionalStructure
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Qual a hora atual ?");
            int hour = int.Parse(Console.ReadLine());

            if(hour < 12)
            {
                Console.WriteLine("Bom dia!");
            }
            else if (hour >=12 && hour < 18)
            {
                Console.WriteLine("Boa tarde!");
            }
            else
            {
                Console.WriteLine("Boa noite!");
            }

        }
    }
}
