﻿using System;

namespace RepetitionStructureFor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Informe quantos números inteiros vai digitar ?");
            int amount = int.Parse(Console.ReadLine());

            int sum = 0;
            for(int i = 1; i <= amount; i++){
                Console.WriteLine("Informe valor {0} : " , i);
                int value = int.Parse(Console.ReadLine());
                sum += value; 
                
            }
            Console.WriteLine("SUM : " + sum);
        }
    }
}
