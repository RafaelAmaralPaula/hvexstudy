﻿using System;

namespace BhaskaraFormula
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 1.0;
            double b = -3.0;
            double c = -4.0;

            double delta = Math.Pow(b , 2) -4 * a * c;


            double x1 = (-b + Math.Sqrt(delta)) / (2 * a);
            double x2 = (-b - Math.Sqrt(delta)) / (2 * a);

            Console.WriteLine("DELTA : " + delta);
            Console.WriteLine("X1 : " + x1);
            Console.WriteLine("X2 : " + x2);

        }
    }

}