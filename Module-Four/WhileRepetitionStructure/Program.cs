﻿using System;
using System.Globalization;

namespace WhileRepetitionStructure
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite um valor : ");    
            double value = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);

            while(value >= 0.0){
                double squareRoot = Math.Sqrt(value);
                Console.WriteLine(squareRoot.ToString("F3" , CultureInfo.InvariantCulture));
                Console.WriteLine("Digite outro valor :");
                value = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            }

            Console.WriteLine("Número negativo");

        }
    }
}
