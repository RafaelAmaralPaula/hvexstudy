﻿using System;

namespace AssignmentOperator
{
    class Program
    {
        static void Main(string [] args)
        {
            int a = 10;
            Console.WriteLine(a);

            a += 2;
            Console.WriteLine(a);

            a *= 3;
            Console.WriteLine(a);

            string name = "Maria";
            name += " Silva";
            Console.WriteLine(name);


            Console.WriteLine("--------------------------------------------------------");

            int b = 10;
            
            //Esse código irá imprimir 10 pois primeiro contempla valor b depois faz incrementação de b
            //int c = b++;
            
            int c = ++b;
            
            Console.WriteLine(b);
            Console.WriteLine(c);

        }
    }


}