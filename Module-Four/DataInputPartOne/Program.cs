﻿using System;

namespace DataInputPartOne
{
    class Program
    {
        static void Main(string[] args)
        {

            string? goodMorning = Console.ReadLine();
            string? redColor = Console.ReadLine();
            string? greenColor = Console.ReadLine();
            string? blueColor = Console.ReadLine();


            Console.WriteLine("Você digitou :");

            Console.WriteLine("");

            Console.WriteLine(goodMorning);
            Console.WriteLine(redColor);
            Console.WriteLine(greenColor);
            Console.WriteLine(blueColor);
            
             Console.WriteLine("------------------------------------------------------------------------");
            //Exemplo usando split para recortar uma frase em partes

            string? phrase = Console.ReadLine();

            
            string[] vector = phrase.Split(' ');
            
            string phraseOne = vector[0];
            string phraseTwo = vector[1];
            string phraseThree = vector[2];

            Console.WriteLine($"Frase 1 : {phraseOne}");
            Console.WriteLine($"Frase 2 : {phraseTwo}");
            Console.WriteLine($"Frase 3 : {phraseThree}");
           
           


        }
    }

}

