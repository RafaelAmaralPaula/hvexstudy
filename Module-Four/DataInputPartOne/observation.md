# Erro na leitura de dados pelo console do VS CODE

## Solução:
- O comando Console.ReadLine(); para funcionar no vs code é preciso,
    alterar configuração do arquivo chamdo **launch.json** trocando valor da propriedade 
    chamada **"console"** para **"integratedTerminal**.

- Isso estava acontecendo porque o console padrão no **VS Code** era o terminal externo. Se você definir o terminal integrado, ele permite que você receba a entrada do usuário.

    [Referência para solução](https://stackoverflow.com/questions/59664418/console-readline-not-working-in-vs-code-writing-a-code-in-c-sharp)