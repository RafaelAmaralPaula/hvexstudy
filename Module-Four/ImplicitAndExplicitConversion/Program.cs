﻿namespace ImplicitAndExplicitConversion
{
    class Program{
        static void Main(string[] args)
        {
            //explicit
            double numberOne;
            float numberTwo;

            numberOne = 5.1f;
            numberTwo = (float)numberOne;

            Console.WriteLine(numberTwo);

            //implicit
            //example one

            float numberThree;
            double numberFour;

            numberThree = 4.3F;
            numberFour = numberThree;

            Console.WriteLine(numberFour);

            //example two
            double numberFive;
            int numberSix;
            
            numberFive = 4.3;
            numberSix = (int)numberFive;

            Console.WriteLine(numberSix);

            //example three
            int numberSeven = 5;
            int numberEight = 2;

            double resultOne = numberSeven / numberEight;

            Console.WriteLine(resultOne);

            //example four 
            int numberNine = 5;
            int numberTen = 2;

            double resultTwo = (double)numberNine / numberTen;
            
            Console.WriteLine(resultTwo);





            

        }

    }
}