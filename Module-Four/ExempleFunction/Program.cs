﻿using System;

namespace ExampleFunction
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite três valores que iremos dizer qual é maior entre eles");
            
            int valueOne = int.Parse(Console.ReadLine());
            int valueTwo = int.Parse(Console.ReadLine());
            int valueThree = int.Parse(Console.ReadLine());

            int result = Larger(valueOne , valueTwo , valueThree);

            Console.WriteLine("MAIOR : " + result);

        }

        static int Larger(int valueOne , int valueTwo , int valueThree)
        {
            int valueLarger = 0;

            if(valueOne > valueTwo && valueOne > valueThree)
            {
                valueLarger = valueOne;
            }
            else if(valueTwo > valueThree)
            {
                valueLarger = valueTwo;
            }
            else
            {
                valueLarger = valueThree;
            }

            return valueLarger;
        }

    }
}