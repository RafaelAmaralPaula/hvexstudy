﻿using System;
using System.Globalization;

namespace DataInputTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = int.Parse(Console.ReadLine());
            char gender = char.Parse(Console.ReadLine());
            double height = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);

            Console.WriteLine("Você digitou :");
            Console.WriteLine(age);
            Console.WriteLine(gender);
            Console.WriteLine(height.ToString("F2" , CultureInfo.InvariantCulture));

            // Lendo tudo em uma linha só
            Console.WriteLine("----------------------------------------------------------------------------");

            string[] vector = Console.ReadLine().Split(' ');

            string name = vector[0];
            string email = vector[1];
            double weight = double.Parse(vector[2]);

            Console.WriteLine("Você digitou :");
            Console.WriteLine(name);
            Console.WriteLine(email);
            Console.WriteLine(weight);


        }
        
    }
}
