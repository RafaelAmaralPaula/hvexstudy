﻿namespace ComparativeOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            // MAIOR E MENOR
            bool exampleOne = 10 > 5;
            bool exampleTwo = 10 < 5;

            Console.WriteLine(exampleOne);
            Console.WriteLine(exampleTwo);
            
            // MAIOR IGUAL E MENOR IGUAL
            bool exampleThree = 10 >= 5;
            bool exampleFour = 10 <= 5;

            Console.WriteLine(exampleThree);
            Console.WriteLine(exampleFour);

            bool exampleFive = 10 >= 10;
            bool exampleSix = 5 <= 5;

            Console.WriteLine(exampleFive);
            Console.WriteLine(exampleSix);


            //IGUALDADE E DIFERENÇA
            bool exampleSeven = 40 == 40;
            bool exampleEight = 40 == 30;

            Console.WriteLine(exampleSeven);
            Console.WriteLine(exampleEight);

            bool exampleNine = 40 != 20;
            bool exampleTen = 50 != 50;

            Console.WriteLine(exampleNine);
            Console.WriteLine(exampleTen);


        }
    }

}