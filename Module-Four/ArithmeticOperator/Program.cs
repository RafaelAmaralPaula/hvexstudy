﻿using System;

namespace ArithmeticOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            //ADIÇÃO
            int numberOneAddition = 20;
            int numberTwoAddition = 10;

            int additionResult = numberOneAddition + numberTwoAddition;
            Console.WriteLine("Resultado da soma entre número {0} e número {1} é igual : {2}" , numberOneAddition , numberTwoAddition , additionResult);

            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------");

            //SUBTRAÇÃO
            int subtractionNumberOne = 40;
            int subtractionNumberTwo = 20;

            int subtractionResult = subtractionNumberOne - subtractionNumberTwo;
            Console.WriteLine($"Resultado da subtração entre número {subtractionNumberOne} e número {subtractionNumberTwo} é igual : {subtractionResult}");
            
            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------");
 

            //MULTIPLICAÇÃO
            int multiplicationNumberOne = 5;
            int multiplicationNumberTwo = 2;

            int multiplicationResult = multiplicationNumberOne * multiplicationNumberTwo;
            Console.WriteLine("Resultado da multiplicação entre número " + multiplicationNumberOne + " e número " + multiplicationNumberTwo + " é igual : " + multiplicationResult);

            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------");

            //DIVISÃO
            int divisionNumberOne = 25;
            int divisionNumberTwo = 5;

            int divisionResult = divisionNumberOne / divisionNumberTwo;
            Console.WriteLine($"Resultado da divisão entre número {divisionNumberOne} e número {divisionNumberTwo} é igual : {divisionResult}");


            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------");

            int divisionNumberThree = 10;
            int divisionNumberFour =  8;

            double divisionResultWithDecimal = (double)divisionNumberThree / divisionNumberFour;
            Console.WriteLine("Resultado da divisão entre número " + divisionNumberThree + " e número " + divisionNumberFour + " é igual : " + divisionResultWithDecimal);

            /*
              SOLUTION POSIBLE 

              O resultado da divisão entre 10 e 8 é equivalente 1,25

              Mais como estamos armazenando resultado em uma variável do tipo inteiro,
              automaticamente compilador entende queremos somente parte inteira igonorando o resto

              ----------------------------------------------------------------------------------
              
              Temos algumas soluções se queremos obter 1,25:

              double divisionResultWithDecimal = (double)divisionNumberThree / divisionNumberFour;

              double divisionNumberThree = 10.0;
              int divisionNumberFour =  8;

              double divisionResultWithDecimal = divisionNumberThree / divisionNumberFour;

              ----------------------------------------------------------------------------------
              double divisionNumberThree = 10.0;
              double divisionNumberFour =  8.0;

              double divisionResultWithDecimal = divisionNumberThree / divisionNumberFour;

            */

            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------");
    
            //RESTO DA DIVISÃO
            int moduleNumberOne = 17;
            int moduleNumberTwo = 3;

            int moduleResult = moduleNumberOne % moduleNumberTwo;
            Console.WriteLine("Resultado do resto da divisão entre número {0} e número {1} é igual : {2} " , moduleNumberOne , moduleNumberTwo , moduleResult);

            
            

        }

    }

}