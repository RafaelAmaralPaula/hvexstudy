﻿using System;

namespace LogicalOperator
{
    class Program
    {
        static void Main(string[] args)
        
        {
            // &&
            bool exampleOperatorAnd = 2 > 3 && 4 != 5;
            Console.WriteLine(exampleOperatorAnd);

            // ||
            bool exampleOperatorOr = 2 > 3 || 4 != 5;
            Console.WriteLine(exampleOperatorOr);

            // !
            bool exampleOperatorNegative = !(2 > 4) && 4 != 5;
            Console.WriteLine(exampleOperatorNegative);

            bool value = 10 < 5;
            Console.WriteLine(value);
            
            bool result = exampleOperatorOr || exampleOperatorNegative && value;
            Console.WriteLine(result);



        }
    }

}
