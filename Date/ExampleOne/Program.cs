﻿using System;

namespace Date
{
    class Program
    {
        static void Main(string[] args)
        {

            var dateOne = new DateTime();
            Console.WriteLine("Date e hora sem inicializar -> " + dateOne);

            var currentDate = DateTime.Now;
            Console.WriteLine("Data e hora atual -> " + currentDate);
        }
    }
}
