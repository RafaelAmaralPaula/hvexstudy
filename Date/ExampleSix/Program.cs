﻿using System.Globalization;

namespace ExampleSix
{
    class Program
    {
        static void Main(string[] args)
        {
            var br = new CultureInfo("pt-BR");
            var pt = new CultureInfo("pt-PT");
            var enUS = new CultureInfo("en-US");
            var enUK = new CultureInfo("en-UK");
            var de = new CultureInfo("d-DE");

            Console.WriteLine(DateTime.Now.ToString("D", enUS));
            Console.WriteLine(DateTime.Now.ToString("D", enUK));
            Console.WriteLine(DateTime.Now.ToString("D", pt));
            Console.WriteLine(DateTime.Now.ToString("D", br));
            Console.WriteLine(DateTime.Now.ToString("D", de));

            var cultureCurrent = CultureInfo.CurrentCulture;
            Console.WriteLine(DateTime.Now.ToString("D", cultureCurrent));

        }
    }
}
