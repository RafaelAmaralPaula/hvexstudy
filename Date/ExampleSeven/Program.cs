﻿using System;

namespace ExampleSeven
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(DateTime.DaysInMonth(2022, 2));

            // SE ÉFIM DE SEMANA OU NÃO
            Console.WriteLine(IsWeek(DateTime.Now.DayOfWeek));

            //Horário verão
            Console.WriteLine(DateTime.Now.IsDaylightSavingTime());
        }

        static bool IsWeek(DayOfWeek today)
        {
            return today == DayOfWeek.Saturday || today == DayOfWeek.Sunday;
        }
    }
}
