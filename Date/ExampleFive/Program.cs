﻿using System;

namespace ExampleFive
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            Uma observção a se fazer é que para quais quer tipo de comparação entre
            datas precisamos estar atento a detalhe pois a comparação é feita de todos elementos
            que compõem uma data como por exemplo até fração de segundos ou seja todos detalhes minimos
            serão usados como critério para comparação
            */

            //Uma outra alternativa seria formatar data para um determinado padrão tirando a 
            //parte das horas.

            var date = DateTime.Now;

            if (date.Date == DateTime.Now.Date)
            {
                Console.WriteLine("É igual...");
            }

            Console.WriteLine(date);

        }
    }
}
