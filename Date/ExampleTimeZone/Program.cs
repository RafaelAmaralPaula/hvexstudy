﻿using System.Globalization;

namespace ExampleTimeZone
{
    class Program
    {
        static void Main(string[] args)
        {
            var utcDate = DateTime.UtcNow;

            Console.WriteLine(DateTime.Now);
            Console.WriteLine(utcDate);

            Console.WriteLine(utcDate.ToLocalTime());

            var timeZoneAustralian = TimeZoneInfo.FindSystemTimeZoneById("Pacific/Auckland");
            Console.WriteLine(timeZoneAustralian);


            var australianHours = TimeZoneInfo.ConvertTimeFromUtc(utcDate, timeZoneAustralian);
            Console.WriteLine(australianHours);

            var timeZones = TimeZoneInfo.GetSystemTimeZones();
            foreach (var timeZone in timeZones)
            {
                Console.WriteLine(timeZone.Id);
                Console.WriteLine(timeZone);
                Console.WriteLine(TimeZoneInfo.ConvertTimeFromUtc(utcDate, timeZone));
                Console.WriteLine("--------------------------------------------------------");
            }



        }
    }
}
