﻿using System;

namespace ExampleTimeSpan
{
    class Program
    {
        static void Main(string[] args)
        {

            var timeSpan = new TimeSpan();
            Console.WriteLine(timeSpan);

            var timeSpanNanoSecond = new TimeSpan(1);
            Console.WriteLine(timeSpanNanoSecond);

            var timeSpanHourMinuteSecond = new TimeSpan(5, 12, 8);
            Console.WriteLine(timeSpanHourMinuteSecond);

            var timeSpanDayHourMinuteSecond = new TimeSpan(3, 5, 50, 10);
            Console.WriteLine(timeSpanDayHourMinuteSecond);

            var timeSpanDayHourMinuteSecondMiliSecond = new TimeSpan(15, 12, 55, 8, 100);
            Console.WriteLine(timeSpanDayHourMinuteSecondMiliSecond);

            Console.WriteLine("----------------------------------------------------------------");
            Console.WriteLine(timeSpanHourMinuteSecond - timeSpanDayHourMinuteSecond);
            Console.WriteLine(timeSpanDayHourMinuteSecond.Days);
            Console.WriteLine(timeSpanDayHourMinuteSecond.Add(new TimeSpan(12, 0, 0)));



        }
    }
}
