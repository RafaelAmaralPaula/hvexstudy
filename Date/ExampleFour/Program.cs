﻿using System;

namespace ExampleFour
{
    class Program
    {
        static void Main(string[] args)
        {
            var date = DateTime.Now;
            Console.WriteLine(date);

            Console.WriteLine(date.AddDays(5));
            Console.WriteLine(date.AddMonths(3));
            Console.WriteLine(date.AddYears(1));

            Console.WriteLine(date.AddHours(2));
            Console.WriteLine(date.AddMinutes(1));
            Console.WriteLine(date.AddSeconds(40));

            Console.WriteLine("-------------------------------------------------");
            date = DateTime.Now;

            Console.WriteLine(date);

            Console.WriteLine(date.AddDays(-2));
            Console.WriteLine(date.AddMonths(-4));
            Console.WriteLine(date.AddYears(-1));

            Console.WriteLine(date.AddHours(-5));
            Console.WriteLine(date.AddMinutes(-50));
            Console.WriteLine(date.AddHours(-20));


        }

    }
}