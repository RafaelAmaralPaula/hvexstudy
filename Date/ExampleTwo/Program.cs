﻿using System;

namespace ExampleTwo
{
    class Program
    {
        static void Main(string[] args)
        {

            var date = new DateTime(2022, 12, 01, 8, 20, 0);
            Console.WriteLine(date);

            Console.WriteLine("----------------------------------");
            Console.WriteLine(date.Day);
            Console.WriteLine(date.Month);
            Console.WriteLine(date.Year);
            Console.WriteLine(date.Hour);
            Console.WriteLine(date.Minute);
            Console.WriteLine(date.Second);

            Console.WriteLine("-----------------------------------");
            Console.WriteLine(date.DayOfWeek);
            Console.WriteLine((int)date.DayOfWeek);

            Console.WriteLine(date.DayOfWeek);
            Console.WriteLine((int)date.DayOfYear);



        }
    }
}
