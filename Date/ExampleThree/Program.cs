﻿using System;

namespace ExampleThree
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
                yyyy = ano
                MM = mês
                dd = dia

                hh = hora
                mm = minuto
                ss = segundo

                f = fração de segundo
                z = time zone do local
            */
            var date = DateTime.Now;
            //var format = String.Format("{0:dd/MM/yyyy hh:mm:ss ff z}", date);

            var format = String.Format("{0:r}", date);

            Console.WriteLine(format);
        }
    }

}
