﻿namespace ExampleFormattingWithDateTime
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var now = DateTime.Now;
            var formats = new string[]{"d" , "D" , "f" , "F" , "g" , "G" , "m" , "o" ,"r" ,
                "s" , "t" , "T" , "u" ,  "U", "Y"};

            foreach (var format in formats)
            {
                Console.WriteLine("Data no formato {0} : {1}", format, now.ToString(format));
                //Console.WriteLine($"Data no formato {format} : {now.ToString(format)}");
            }

            Console.WriteLine("Diferentes maneiras de formatar manualmente.");
            Console.WriteLine($"Data no formato d: {now:d}");
            Console.WriteLine($"Data no formato MM-dd-yyyy: {now:MM-dd-yyyy}");

            var dateFormat = now.ToString("MM-dd-yyyy");
            Console.WriteLine(dateFormat);

            var dateFormatBr = now.ToString("dd/MM/yyyy HH:mm:ss");
            Console.WriteLine(dateFormatBr);

        }
    }
}
