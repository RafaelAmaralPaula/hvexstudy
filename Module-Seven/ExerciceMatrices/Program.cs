﻿using System;

namespace ExerciceMatrices
{
    class Program
    {
        static void Main(string[] args)
        {
            int lenghtMatrices = int.Parse(Console.ReadLine());

            int[,] matrices = new int[lenghtMatrices , lenghtMatrices];

            for(int i = 0 ; i < lenghtMatrices; i++){
                string[] values = Console.ReadLine().Split(' ');
                for(int j = 0 ; j < lenghtMatrices ; j++){
                    matrices[i,j] = int.Parse(values[j]);
                }
            }

            Console.WriteLine("Main diagonal");
            for(int i = 0; i < lenghtMatrices; i++){
                Console.Write(matrices[i,i] + " ");
            }

            Console.WriteLine();

            int count = 0;
            for(int i = 0; i < lenghtMatrices; i++){
                for(int j = 0 ; j < lenghtMatrices ; j++){
                    if(matrices[i , j ] < 0){
                        count ++;
                    }
                }
            }
            Console.WriteLine("Negative numbers : " + count);
        }
    }
}
