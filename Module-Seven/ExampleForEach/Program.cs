﻿namespace ExampleForEach
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] vector = new String[]{"João" , "Pedro" , "Daniel"};

            foreach(string result in vector)
            {
                Console.WriteLine(result);
            }
        }
    }
}
