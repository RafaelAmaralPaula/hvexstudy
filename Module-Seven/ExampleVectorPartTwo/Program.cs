﻿using System;
using System.Globalization;
using ExampleVectorPartTwo.Model;

namespace ExampleVectorPartTwo
{
    public class Program
    {
        static void Main(string[] args)
        {

            int lenghtVector = int.Parse(Console.ReadLine());

            Product[] products = new Product[lenghtVector];

            for(int i = 0; i < lenghtVector; i++){
                Console.WriteLine("Name product : " + i);
                string name = Console.ReadLine();

                Console.WriteLine("Price product : " + i);
                double price = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);

                products[i] = new Product{Name = name , Price = price};
            }

            double sum = 0.0;

            for(int i = 0 ; i< products.Length; i++){
                sum += products[i].Price;
            }

            double avg = sum / lenghtVector;

            Console.WriteLine("Média = " + avg.ToString("F2" , CultureInfo.InvariantCulture));

        }
    }

}
