﻿using System;
using System.Globalization;

namespace ExampleVectorPartOne
{
    class Program
    {
        static void Main(string[] args)
        {

            int vectorLenght = int.Parse(Console.ReadLine());

            double[] vector = new double[vectorLenght];

            for(int i = 0; i < vectorLenght ; i++){
                vector[i] = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
            }

            double sum = 0.0;
            for(int i = 0; i < vector.Length; i++){
                sum += vector[i];
            }

            double avg = sum / vectorLenght;

            Console.WriteLine("MÉDIA = " + avg.ToString("F2") , CultureInfo.InvariantCulture);


        }
    }
}
