﻿using ExampleModifierParamsRef.Model;

namespace ExampleModifierParamsRef
{
    class Program
    {
        static void Main(string[] args)
        {

            int a = 10;
            Calculator.Sum(ref a);

            Console.WriteLine(a);

        }
    }
}