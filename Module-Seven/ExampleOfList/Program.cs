﻿using System.Collections.Generic;
namespace ExampleOfList
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>();
            list.Add("Aria");
            list.Insert(1 , "Marta");

            Console.WriteLine("COUNT: " + list.Count);

            string s1 = list.Find(X => X[0] == 'A');
            Console.WriteLine(s1);

            string s2 = list.FindLast(X => X[0] == 'A');
            Console.WriteLine(s2);

            int p1 = list.FindIndex(X => X[0] == 'A');
            Console.WriteLine(p1);

            int p2 = list.FindLastIndex(X => X[0] == 'A');
            Console.WriteLine(p2);

            List<string> result = list.FindAll(X => X.Length == 5);
            foreach(string obj in result)
            {
                Console.WriteLine(obj);
            }
            

            list.Remove("Marta");
            foreach(string obj in list)
            {
                Console.WriteLine(obj);
            }


            list.RemoveAll(X => X[0] == 'M');
            foreach(string obj in list)
            {
                Console.WriteLine(obj);
            }

            list.RemoveAt(0);
            foreach(string obj in list)
            {
                Console.WriteLine(obj);
            }

            list.Add("Gabriel");
             list.Add("Pedro");

            // A PARTIR DE QUAL POSIÇÃO , E QUANTIDADE QUE QUER REMOVER
            list.RemoveRange(0 , 1);
             foreach(string obj in list)
            {
                Console.WriteLine(obj);
            }

        }

    }
}
