﻿namespace ExampleMatrices
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] matrices = new double[2 , 3];

            Console.WriteLine(matrices.Length);

            Console.WriteLine(matrices.Rank);

            Console.WriteLine(matrices.GetLength(0));
            
            Console.WriteLine(matrices.GetLength(1));

        }
    }

}