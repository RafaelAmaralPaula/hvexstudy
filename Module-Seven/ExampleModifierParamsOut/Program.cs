﻿using ExampleModifierParamsOut.Model;

namespace ExampleModifierParamsOut
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 10;
            int triple = 0;
            Calculator.Sum(a , out triple);
            Console.WriteLine(triple);
        }
    }
}