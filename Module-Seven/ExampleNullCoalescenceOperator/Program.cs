﻿namespace ExampleNullCoalescenceOperator
{
    class Program
    {
        static void Main(string[] args)
        {
            double? x = null;
            double? y = 60;

            double a = x ?? 5;
            double b = y ?? 10;

            Console.WriteLine(a);
            Console.WriteLine(b);

        }    
    }
}