﻿namespace ExampleNullable
{
    class Program
    {
        static void Main(string[] args)
        {
            double? x = null;
            double? y = 10;

            Console.WriteLine(x.GetValueOrDefault());
            Console.WriteLine(y.GetValueOrDefault());

            Console.WriteLine(x.HasValue);
            Console.WriteLine(y.HasValue);

            /*LANÇA UMA EXCEPTION
            Console.WriteLine(x.Value);
            Console.WriteLine(y.Value);
            */

            if(x.HasValue)
            {
                 Console.WriteLine(x.Value);
            }
            else
            {
                 Console.WriteLine("X is null");
            }

            //Y

            if(y.HasValue)
            {
                 Console.WriteLine(y.Value);
            }
            else
            {
                Console.WriteLine("Y is null");
            }

        }
    }
}