﻿namespace ExampleOne
{
    class Program
    {
        public static void Main(string[] args)
        {
            var paragraph = "       C# é uma linguagem moderna e versátil. " +
                "Com C# consigo desenvolver para Web, Desktop, Jogos, " +
                "Monile , entre outros.         ";

            Console.WriteLine("--------------------------------------------------------------");
            //LENGHT
            var size = paragraph.Length;
            Console.WriteLine(size);

            //EMPTY
            var empty = string.Empty;

            Console.WriteLine("--------------------------------------------------------------");

            //ToLower , ToUpper
            var toLowerParagraph = paragraph.ToLower();
            var toUpperParagraph = paragraph.ToUpper();

            Console.WriteLine(toLowerParagraph);

            Console.WriteLine("--------------------------------------------------------------");

            Console.WriteLine(toUpperParagraph);

            //Split
            var splitParagraph = paragraph.Split(".");

            Console.WriteLine("--------------------------------------------------------------");

            Console.WriteLine(splitParagraph);

            //Trim , TrimEnd , TrimStart
            var toTrimParagraph = paragraph.Trim();
            var toTrimEndParagraph = paragraph.TrimStart();
            var toTrimStartParagraph = paragraph.TrimEnd();

            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine(toTrimParagraph);


            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine(toTrimStartParagraph);

            Console.WriteLine("--------------------------------------------------------------");
            Console.WriteLine(toTrimEndParagraph);


            Console.WriteLine("--------------------------------------------------------------");

            //Replace
            var replaceParagraph = paragraph.Replace("C#", "C-sharp");
            Console.WriteLine(replaceParagraph);

            Console.WriteLine("--------------------------------------------------------------");

            //IsNullOrWhiteSpace

            var isNullOrWhiteSpaces = string.IsNullOrWhiteSpace(paragraph);
            Console.WriteLine(isNullOrWhiteSpaces);

        }


    }
}
