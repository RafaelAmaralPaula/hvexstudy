﻿namespace ExalpleSearchWithString
{
    class Program
    {
        public static void Main(string[] args)
        {
            var paragraph = "C# é uma linguagem moderna e versátil. " +
                    "Com C# consigo desenvolvder para Web , Desktop , Jogos, " +
                    "Mobile, entre outros.";

            //IndexOf
            var indexOf = paragraph.IndexOf("C#");

            //LastIndexOf
            var lastOf = paragraph.LastIndexOf("C#");

            //StartWith
            var startWith = paragraph.StartsWith("C#");

            //SubString
            var indexOfMobile = paragraph.IndexOf("Mobile");
            var subString = paragraph.Substring(indexOfMobile, "Mobile".Length);
            Console.WriteLine(subString);

            //Contains
            var containsPlay = paragraph.Contains("jogos", StringComparison.OrdinalIgnoreCase);
            var containsPlayExact = paragraph.Contains("Jogos");
            var noContains = paragraph.Contains("ruim");

        }
    }
}
