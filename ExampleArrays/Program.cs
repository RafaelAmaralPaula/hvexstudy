﻿namespace ExampleArrays
{
    public class Program
    {
        public static void Main(string[] args)
        {

            //Arrays
            var numbersExempleOne = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var numbersCopy = new int[10];

            for (int i = 0; i < numbersExempleOne.Length; i++)
            {
                numbersCopy[i] = numbersExempleOne[i];
                Console.WriteLine(numbersCopy[i]);
            }

            Console.WriteLine("--------------------------------------------------------------------------");

            var stringNumbers = "0 1 2 3 4 5 6 7 8 9 10 11";
            var numbersArraysInt = stringNumbers.Split(" ");
            var numbersConvertoFromInt = Array.ConvertAll(numbersArraysInt, Convert.ToInt32);

            for (int i = 0; i < numbersConvertoFromInt.Length; i++)
            {
                var last = numbersConvertoFromInt.Last();
                if (i != last)
                {
                    Console.Write(numbersConvertoFromInt[i] + " , ");
                }
                else
                {
                    Console.Write(numbersConvertoFromInt[i]);
                }

            }

        }
    }
}