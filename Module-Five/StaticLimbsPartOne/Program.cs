﻿using System;
using System.Globalization;

namespace StaticLimbsPartOne
{
    class Program
    {
        public const double PI = 3.14;
        static void Main(string[] args)
        {
            Console.Write("Entre com valor do raio:");
            double lightning = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);

            double circumference = CircumferenceCalculator(lightning);
            double volume = Volume(lightning);

            Console.WriteLine("Circuferência: " + circumference.ToString("F2" , CultureInfo.InvariantCulture));
            Console.WriteLine("Volume: " + volume.ToString("F2" , CultureInfo.InvariantCulture));
            Console.WriteLine("PI: " + PI.ToString("F2" , CultureInfo.InvariantCulture));


        }

        public static double CircumferenceCalculator(double lightning)
        {
            return 2.0 * PI * lightning;

        }

        public static double Volume(double linghtning)
        {   
            return 4.0 / 3.0 * PI * Math.Pow(linghtning , 3);
        }
    }

}