﻿using System;
using System.Globalization;
using AppProduct.Model;


namespace AppProduct
{
    class Program 
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entre com dados de produtos : ");

            Product product = new Product();

            Console.Write("Nome : ");
            product.Name = Console.ReadLine();

            Console.Write("Preço : ");
            product.Price = double.Parse(Console.ReadLine() ,  CultureInfo.InvariantCulture);

            Console.Write("Quantidade em estoque : ");
            product.Quantity = int.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
        
            Console.WriteLine();
            Console.WriteLine($"Dados do produto : " + product);

            Console.WriteLine();
            Console.WriteLine("Digite o número de produtos a ser adicionado ao estoque : ");
            int quantity = int.Parse(Console.ReadLine());
            product.AddProduct(quantity);

            Console.WriteLine("");
            Console.WriteLine("Dados atualizado: " + product.ToString());
            
            Console.WriteLine();
            Console.WriteLine("Digite o número de produtos a ser removido do estoque : ");
            quantity = int.Parse(Console.ReadLine());
            product.RemoveProduct(quantity);

            Console.WriteLine();
            Console.WriteLine("Dados atualizado : " + product.ToString());


        }

    }

}