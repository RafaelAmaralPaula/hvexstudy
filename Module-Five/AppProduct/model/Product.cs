using System;
using System.Globalization;

namespace AppProduct.Model
{
    class Product
    {
        public string Name;
        public double Price;
        public int Quantity;

        public double TotalValueStock()
        {
            return Price * Quantity;
        }

        public void AddProduct(int quantity)
        {
            Quantity += quantity;
        }

        public void RemoveProduct(int quantity)
        {
            Quantity -= quantity;
        }

        public override string ToString()
        {
            return Name 
            + ", $" 
            + Price.ToString("F2" , CultureInfo.InvariantCulture) 
            + ", "
            + Quantity
            + " unidades , Total : $"
            + TotalValueStock().ToString("F2" , CultureInfo.InvariantCulture);
        }

    }
}