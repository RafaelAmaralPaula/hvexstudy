﻿using System;
using System.Globalization;

namespace  ExampleCalculateAreaTriangleWithoutObjectOrientation
{
    class Program
    {
        static void Main(string[] args)
        {
            
            double sideXA , sideXB , sideXC;
            double sideYA , sideYB , sideYC;

            Console.WriteLine("Entre com as medidas do triângulo X: ");
            sideXA = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
            sideXB = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
            sideXC = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);

            Console.WriteLine("Entre com as medidas do triângulo Y: ");
            sideYA = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
            sideYB = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
            sideYC = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);

            double p = (sideXA + sideXB + sideXC ) / 2.0;
            double areaX = Math.Sqrt(p * (p - sideXA) * (p - sideXB) * (p - sideXC));

            p = (sideYA + sideYB + sideYC) / 2.0;
            double areaY = Math.Sqrt(p * (p - sideYA) * (p - sideYB) * (p - sideYC));

            Console.WriteLine("Área de X: " + areaX.ToString("F4" , CultureInfo.InvariantCulture));
            Console.WriteLine("Área de Y: " + areaY.ToString("F4"   , CultureInfo.InvariantCulture));

            if(areaX > areaY)
            {
                Console.WriteLine("Maior área: X");
            }
            else
            {
                Console.WriteLine("Maior área: Y");
            }

        }
    }
    
}