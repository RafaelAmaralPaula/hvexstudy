﻿using System;
using System.Globalization;
using StaticLimbsPartTwo.Model;

namespace StaticLimbsPartTwo
{
    class Program
    {
        static void Main(string[] args)
        {
    
            Console.WriteLine("Entre com o valor do raio: ");
            double lightning = double.Parse(Console.ReadLine() , CultureInfo.InvariantCulture);
            
            double circumference = Calculator.CircumferenceCalculator(lightning);
            double volume = Calculator.Volume(lightning);

            Console.WriteLine("Circunferência: " + circumference.ToString("F2" , CultureInfo.InvariantCulture));
            Console.WriteLine("Volume: " + volume.ToString("F2" , CultureInfo.InvariantCulture));
            Console.WriteLine("PI: " + Calculator.PI.ToString("F2" , CultureInfo.InvariantCulture));

        }

    }

}