namespace StaticLimbsPartTwo.Model
{
    class Calculator
    {

        public const double PI = 3.14;

        public static double CircumferenceCalculator(double lightning)
        {
            return 2.0 * PI * lightning;
        }

        public static double Volume(double lightning)
        {
            return 4.0 / 3.0 * PI * Math.Pow(lightning , 3);
        }


    }

}