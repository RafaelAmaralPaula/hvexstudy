﻿
namespace ExampleSwitchCase
{
    class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Digite 1 - Contratação de um plano.");
            Console.WriteLine("Digite 2 - Reclamação de serviço prestado.");
            Console.WriteLine("Digite 3 - Segunda via de boleto.");
            Console.WriteLine("Digite 4 - Sair.");
            var typeValue = Console.ReadLine();

            switch (typeValue)
            {
                case "1":
                    Console.WriteLine("Informações de plano novo.");
                    break;

                case "2":
                    Console.WriteLine("Fale sobre a reclamação.");
                    break;

                case "3":
                    Console.WriteLine("Segunda via de boleto enviado por e-mail.");
                    break;

                case "4":
                    Console.WriteLine("Tenha um bom dia!");
                    break;

                default:
                    Console.WriteLine("Opção não encontrada.");
                    break;
            }

        }
    }
}