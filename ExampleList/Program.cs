﻿namespace ExempleList
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var array = new int[] { 1, 2, 3, 4, 5 };
            var myLlist = new List<int>();

            var list = new List<int>(array);
            //Add e AddRange
            list.Add(6);
            list.AddRange(new List<int> { 7, 8, 9, 10 });

            //contains
            var containsNumberEleven = list.Contains(11);
            Console.WriteLine(containsNumberEleven);

            var conatinsNumberTwo = list.Contains(2);
            Console.WriteLine(conatinsNumberTwo);

            //foreach , sort e reverse
            list.Reverse();
            Console.WriteLine("Lista reversa : ");
            list.ForEach(l => Console.WriteLine(l));

            Console.WriteLine("----------------------------------------------");

            Console.WriteLine("Lista ordenada : ");
            list.Sort();
            list.ForEach(l => Console.WriteLine(l));

            Console.WriteLine("----------------------------------------------");

            //count
            var countList = list.Count();
            Console.WriteLine(countList);

            Console.WriteLine("----------------------------------------------");

            //remove

            list.Remove(5);
            list.ForEach(l => Console.WriteLine(l));

            Console.WriteLine("----------------------------------------------");

            //removeAll
            list.RemoveAll(l => l < 4);
            list.ForEach(l =>
            {
                Console.WriteLine(l);
            });

            Console.WriteLine("----------------------------------------------");

            //clear

            list.Clear();


        }
    }
}
