﻿using System.IO;

namespace ExampleDirectory
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var folderName = "pasta";
            var subFolderName = "subpasta";
            var subFolderNameStatic = "subpasta_usingstatic";

            var directoryInfoSubFolder = new DirectoryInfo(subFolderName);

            if (!Directory.Exists(folderName) || !directoryInfoSubFolder.Exists)
            {
                Directory.CreateDirectory(subFolderNameStatic);
                Directory.CreateDirectory(folderName);
                directoryInfoSubFolder.Create();

                directoryInfoSubFolder.MoveTo($"{folderName}//{subFolderName}");
                Directory.Move(subFolderNameStatic, @$"{folderName}\{subFolderNameStatic}");
            }

            var name = directoryInfoSubFolder.Name;
            var parent = directoryInfoSubFolder.Parent;
            var root = directoryInfoSubFolder.Root;
            var exist = directoryInfoSubFolder.Exists;

            Console.WriteLine("Nome : " + name);
            Console.WriteLine("Parent : " + parent);
            Console.WriteLine("Root : " + root);
            Console.WriteLine("Exists : " + exist);

            foreach (var diretorie in Directory.GetDirectories(folderName))
            {
                Console.WriteLine(diretorie);

            }

            Directory.Delete(@$"{folderName}\{subFolderName}");

        }
    }
}
